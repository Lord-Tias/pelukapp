<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

use App\Models\Cliente;
use App\Models\User;

use App\Rules\CustomPasswordValidation;

class Cliente_Controller extends Controller
{
    function Registrar(Request $request){
        try {
            $this->validate($request, [
                "email"         => 'required|email',
                "nombre"        => 'required|between:3,30',
                "apellido"      => 'required|between:3,30',
                "telefono"      => 'required|digits_between:8,15',
                "nacimiento"    => 'required|date_format:Y-m-d',
                "contraseña"    => [
                                    'required',
                                    'confirmed',
                                    'string',
                                    new CustomPasswordValidation(),
                                    ],
            ]);

            return Cliente::createCliente($request);

        }
        catch (ValidationException $ve) {
            return ["Message" => "ERROR", "Errores" => $ve->errors()];
        }
    }

    function getClienteReservas(Request $request){
        try {
            $usuario    = Auth::user();

            $cliente    = Cliente::find($usuario->id);
    
            return ["Message" => "OK", "Datos" => $cliente->reservas];
        } catch (QueryException $qe) {
            return ["Message" => "ERROR", "Error" => $qe];
        } catch (\Throwable $th) {
            return ["Message" => "ERROR", "Error" => "Ocurrio un error."];
        }
    }

    function getClientes(Request $request){
        try {
            $clientes    = Cliente::getClientes();
    
            return ["Message" => "OK", "Clientes" => $clientes];
        } catch (QueryException $qe) {
            return ["Message" => "ERROR", "Error" => $qe];
        } catch (\Throwable $th) {
            return ["Message" => "ERROR", "Error" => $th];
        }
    }

    function deleteCliente(Request $request){
        try {
            $ids  = $request->clientes_id;
            if(is_array($ids)){
                foreach($ids as $id){
                    $user = User::findOrFail($id);
                    if($user)
                        $user->delete();
                    else
                        return ["Message" => "ERROR", "Error" => "No se pudo encontrar el Cliente"];
                }
                return ["Message" => "OK"];
            }else{
                return ["Message" => "ERROR", "Error" => "Ocurrio un error."];
            }
        } catch (QueryException $qe) {
            return ["Message" => "ERROR", "Error" => $qe];
        } catch (\Throwable $th) {
            return ["Message" => "ERROR", "Error" => "Ocurrio un error."];
        }
    }
}