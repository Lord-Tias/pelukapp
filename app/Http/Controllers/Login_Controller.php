<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Peluquero;
use App\Models\Cliente;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

use App\Rules\CustomPasswordValidation;

class Login_Controller extends Controller
{
    function Login(Request $request){
        $credentials = $request->only(['email', 'password']);

        $token = auth()->attempt($credentials);

        if($token == ''){
            return ["Message" => "ERROR", "Errores" => "Usuario y/o contraseña incorrecta."];
        }

        $usu = Auth::user();
        $rol = $usu->rol;
        $act = $rol->actions;

        $rol_info = [];

        switch ($usu->rol_id) {
            case 1:
                $rol_info = ["Sin Información."];
                break;
            case 2:
                $rol_info = Peluquero::find($usu->id);
                break;
            case 3:
                $rol_info = Cliente::find($usu->id);
                break;
        }

        return ["Message"   => "OK",
                "Token"     => $token,
                "Usuario"   => $usu,
                "Rol_Info"  => $rol_info
            ];
    }

    function Logout(Request $request){
        auth()->guard('api')->logout();

        return response()->json([
            'status' => 'success'
        ]);
    }

    function Registrar(Request $request){
        try {
            $this->validate($request, [
                "nombre"        => 'required|between:3,30',
                "apellido"      => 'required|between:3,30',
                "email"         => 'required|email',
                "contraseña"    => [
                                    'required',
                                    'confirmed',
                                    'string',
                                    new CustomPasswordValidation(),
                                    ],
            ]);

            User::create([
                'nombre'        => $request->nombre,
                'apellido'      => $request->apellido,
                'email'         => $request->email,
                'password'      => Hash::make($request->contraseña),
                'rol_id'        => 1 /* Administrador */
            ]);

            return ["Message" => "OK"];

        }
        catch (ValidationException $ve) {
            return ["Message" => "ERROR", "Errores" => $ve->errors()];
        }
        catch (QueryException $qe) {
            return ["Message" => "ERROR", "Errores" => ['email' => 'Email ya registrado.']];
        }
    }

    function CambioContraseña(Request $request){
        try {
            $credentials    = $request->only(['email', 'password']);
            $token          = auth()->attempt($credentials);

            if($token == ''){
                return ["Message" => "ERROR", "Errores" => ["password" => "Contraseña actual es incorrecta."]];
            }

            $this->validate($request, [
                "contraseñaNueva"   => [
                                        'required',
                                        'confirmed',
                                        'string',
                                        new CustomPasswordValidation(),
                                        ],
            ]);
            
            User::find(auth()->user()->id)
                ->update([
                            'password'=> Hash::make($request->contraseñaNueva)
                        ]);
            
            return ["Message" => "OK"];
        }
        catch (ValidationException $ve) {
            return ["Message" => "ERROR", "Errores" => $ve->errors()];
        }
        catch (QueryException $qe) {
            return ["Message" => "ERROR", "Errores" => $qe];
        }
    }

    function CambioContraseñaAdmin(Request $request){
        try {
            $id    = $request->usuario_id;

            $this->validate($request, [
                "contraseñaNueva"   => [
                                        'required',
                                        'confirmed',
                                        'string',
                                        new CustomPasswordValidation(),
                                        ],
            ]);
            
            User::find($id)
                ->update([
                            'password'=> Hash::make($request->contraseñaNueva)
                        ]);
            
            return ["Message" => "OK"];
        }
        catch (ValidationException $ve) {
            return ["Message" => "ERROR", "Errores" => $ve->errors()];
        }
        catch (QueryException $qe) {
            return ["Message" => "ERROR", "Errores" => $qe];
        }
    }
}