<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Peluquero;
use App\Models\User;
use App\Models\Reserva;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

use App\Rules\CustomPasswordValidation;

class Peluquero_Controller extends Controller
{
    function Registrar(Request $request){
        try {
            $this->validate($request, [
                "nombre"            => 'required|between:3,30',
                "apellido"          => 'required|between:3,30',
                "email"             => 'required|email',
                "inicio_actividad"  => 'required|date_format:H:i',
                "fin_actividad"     => 'required|date_format:H:i|after:inicio_actividad',
                "contraseña"        => [
                                        'required',
                                        'confirmed',
                                        'string',
                                        new CustomPasswordValidation(),
                                        ],
            ]);

            return Peluquero::createPeluquero($request);
            //return ["Message" => "OK"];

        }
        catch (ValidationException $ve) {
            return ["Message" => "ERROR", "Errores" => $ve->errors()];
        }
    }

    function getPeluqueros(Request $request){
        return ["Message" => "OK", "Peluqueros" => Peluquero::getPeluqueros()];
    }

    function getHorasLibres(Request $request){
        $id         = $request->id_peluquero;
        $fecha      = $request->fecha;
        
        $peluquero  = Peluquero::find($id);

        return ["Message" => "OK", "Datos" => $peluquero->bloquesLibres($fecha)];
    }

    function getPeluqueroReservas(Request $request){
        try {
            $usuario    = Auth::user();

            $peluquero  = Peluquero::find($usuario->id);

            if($peluquero == null){
                return ["Message" => "ERROR",
                        "Error" => "No es usted un Peluquero.s"];
            }else{
                $reservas   = Reserva::where('peluquero_id', $peluquero->id)
                                        ->get()
                                        ->load(['cliente','user_cliente']);
                return ["Message" => "OK", "Datos" => $reservas];
            }
        } catch (QueryException $qe) {
            return ["Message" => "ERROR", "Error" => $qe];
        } catch (\Throwable $th) {
            return ["Message" => "ERROR", "Error" => "Ocurrio un error."];
        }
    }

    function getPeluqueroReservasPorDia(Request $request){
        try {
            $usuario    = Auth::user();

            $peluquero  = Peluquero::find($usuario->id);


            if($peluquero == null){
                return ["Message" => "ERROR",
                        "Error" => "No es usted un Peluquero.s"];
            }else{
                //$reservas   = Reserva::where('peluquero_id', $peluquero->id)->get()->load(['cliente','user_cliente']);
                $reservas   = Reserva::getReservasPeluqueroYDia($peluquero->id,$request->fecha);
                return ["Message" => "OK", "Datos" => $reservas];
            }
        } catch (QueryException $qe) {
            return ["Message" => "ERROR", "Error" => $qe];
        } catch (\Throwable $th) {
            return ["Message" => "ERROR", "Error" => "Ocurrio un error."];
        }
    }

    function deletePeluquero(Request $request){
        try {
            $ids  = $request->peluqueros_id;
            if(is_array($ids)){
                foreach($ids as $id){
                    $user = User::findOrFail($id);
                    if($user)
                        $user->delete();
                    else
                        return ["Message" => "ERROR", "Error" => "No se pudo encontrar el Cliente"];
                }
                return ["Message" => "OK"];
            }else{
                return ["Message" => "ERROR", "Error" => "Ocurrio un error."];
            }
        } catch (QueryException $qe) {
            return ["Message" => "ERROR", "Error" => $qe];
        } catch (\Throwable $th) {
            return ["Message" => "ERROR", "Error" => "Ocurrio un error."];
        }
    }
}
