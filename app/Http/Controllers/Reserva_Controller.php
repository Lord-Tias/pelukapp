<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reserva;
use Illuminate\Validation\ValidationException;

class Reserva_Controller extends Controller
{
    function RegistrarLicencia(Request $request){
        try {
            $this->validate($request, [
                "peluquero_id"  => 'required',
                "fecha"         => 'required|date_format:Y-m-d',
                "inicio"        => 'required|date_format:H:i',
                "fin"           => 'required|date_format:H:i|after:inicio'
            ]);

            return Reserva::createLicencia($request);
        }
        catch (ValidationException $ve) {
            return ["Message" => "ERROR", "Errores" => $ve->errors()];
        }
    }

    function Registrar(Request $request){
        try {
            $this->validate($request, [
                "peluquero_id"  => 'required',
                "cliente_id"    => 'required',
                "fecha"         => 'required|date_format:Y-m-d',
                "inicio"        => 'required|date_format:H:i'
            ]);

            return Reserva::createReserva($request);
        }
        catch (ValidationException $ve) {
            return ["Message" => "ERROR", "Errores" => $ve->errors()];
        }
    }
    function RegistrarLicenciaRango(Request $request){
        try {
            $this->validate($request, [
                "peluquero_id"  => 'required',
                "fechaInicio"   => 'required|date_format:Y-m-d',
                "fechaFin"      => 'required|date_format:Y-m-d|after:fechaInicio'
            ]);

            return Reserva::createLicenciaRango($request);
        }
        catch (ValidationException $ve) {
            return ["Message" => "ERROR", "Errores" => $ve->errors()];
        }
    }

    function deleteReserva(Request $request){
        try {
            $reserva = Reserva::findOrFail($request->id_reserva);
            if($reserva)
                $reserva->delete(); 
            else
                return ["Message" => "ERROR", "Error" => "No se pudo encontrar la Reserva"];
            return ["Message" => "OK"];
        } catch (QueryException $qe) {
            return ["Message" => "ERROR", "Error" => $qe];
        } catch (\Throwable $th) {
            return ["Message" => "ERROR", "Error" => "Ocurrio un error."];
        }
    }

    function deleteMultiple(Request $request){
        try {
            $ids  = $request->reservas_id;
            if(is_array($ids)){
                foreach($ids as $id){
                    $user = Reserva::findOrFail($id);
                    if($user)
                        $user->delete();
                    else
                        return ["Message" => "ERROR", "Error" => "No se pudo encontrar la Reserva."];
                }
                return ["Message" => "OK"];
            }else{
                return ["Message" => "ERROR", "Error" => "Ocurrio un error."];
            }
        } catch (QueryException $qe) {
            return ["Message" => "ERROR", "Error" => $qe];
        } catch (\Throwable $th) {
            return ["Message" => "ERROR", "Error" => "Ocurrio un error."];
        }
    }

    function getReservas(Request $request){
        try {
            $hoy                = getdate();
            $anteriorDomingo    = strtotime("-" . $hoy["wday"] . " days");
            $laFecha            = date("Y-m-d", $anteriorDomingo);

            $reservas           = Reserva::getReservasMayoresADia($laFecha);
            
            return ["Message" => "OK", "Reservas" => $reservas];
        } catch (QueryException $qe) {
            return ["Message" => "ERROR", "Error" => $qe];
        } catch (\Throwable $th) {
            return ["Message" => "ERROR", "Error" => "Ocurrio un error."];
        }
    }

    function getLicencias(Request $request){
        try {
            $hoy        = date_create();
            $laFecha    = date_format($hoy,"Y-m-d");

            $Licencias  = Reserva::getLicenciasMayoresADia($request->peluquero_id, $laFecha);
            
            return ["Message" => "OK", "Licencias" => $Licencias];
        } catch (QueryException $qe) {
            return ["Message" => "ERROR", "Error" => $qe];
        } catch (\Throwable $th) {
            return ["Message" => "ERROR", "Error" => "Ocurrio un error."];
        }
    }

    function getLicenciasPorFecha(Request $request){
        try {

            $Licencias = Reserva::getLicenciasDelDia($request->peluquero_id, $request->fecha);
            
            return ["Message" => "OK", "Licencias" => $Licencias];
        } catch (QueryException $qe) {
            return ["Message" => "ERROR", "Error" => $qe];
        } catch (\Throwable $th) {
            return ["Message" => "ERROR", "Error" => "Ocurrio un error."];
        }
    }
}
