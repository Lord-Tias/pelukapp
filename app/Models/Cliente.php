<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;

use App\Models\User;
use App\Models\Reserva;

class Cliente extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'telefono',
        'nacimiento',
    ];
    /**
     * Atributo que especifica que la primary key no es 
     * autoincremental.
     */
    public $incrementing = false;

    public function user(){
        return $this->belongsTo(User::class, 'id');
    }

    public function reservas(){
        return $this->hasMany(Reserva::class)->with('peluquero');
    }

    public static function createCliente(Request $request){
        try {
            $user = User::create([
                'email'         => $request->email,
                'password'      => Hash::make($request->contraseña),
                'nombre'        => $request->nombre,
                'apellido'      => $request->apellido,
                'rol_id'        => 3
            ]);

            Cliente::create([
                'id'            => $user->id,
                'telefono'      => $request->telefono,
                'nacimiento'    => $request->nacimiento
            ]);

            return ["Message" => "OK"];
        }
        catch (QueryException $qe) {
            //return ["Message" => $qe];
            return ["Message" => "ERROR", "Errores" => ['email' => 'Email ya registrado.']];
        }
    }

    public static function getClientes(){
        return Cliente::with('user')->where("id","!=",1)->get();
    }
}
