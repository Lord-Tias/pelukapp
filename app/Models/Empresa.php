<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Peluquero;

class Empresa extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'direccion',
        'ubicacion',
        'mensaje',
        'duracion_servicio'
    ];

    public function peluqueros(){
        return $this->hasMany(Peluquero::class);
    }
}
