<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\QueryException;

use App\Models\User;
use App\Models\Empresa;
use App\Models\Reserva;

use DateInterval;

class Peluquero extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'dias_nolaborables',
        'inicio_actividad',
        'fin_actividad',
        'empresa_id'
    ];
    /**
     * Atributo que especifica que la primary key no es 
     * autoincremental.
     */
    public $incrementing = false;

    public function user(){
        return $this->belongsTo(User::class, 'id');
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function reservas(){
        return $this->hasMany(Reserva::class);
    }

    public static function createPeluquero(Request $request){
        try {
            $user = User::create([
                'nombre'        => $request->nombre,
                'apellido'      => $request->apellido,
                'email'         => $request->email,
                'password'      => Hash::make($request->contraseña),
                'rol_id'        => 2 /* Peluquero */
            ]);

            Peluquero::create([
                'id'                => $user->id,
                'dias_nolaborables' => $request->dias_nolaborables,
                'inicio_actividad'  => $request->inicio_actividad,
                'fin_actividad'     => $request->fin_actividad,
                'empresa_id'        => 1
            ]);

            return ["Message" => "OK"];
        }
        catch (QueryException $qe) {
            //return ["Message" => $qe];
            return ["Message" => "ERROR", "Errores" => ['email' => 'Email ya registrado.']];
        }
    }

    public static function getPeluqueros(){
        return Peluquero::with('user')->get();
    }

    public function bloquesLibres($fecha){

        $date_inicio    = date_create($this->inicio_actividad);
        $date_fin       = date_create($this->fin_actividad);

        $seconds        = strval(60 * $this->empresa->duracion_servicio);
        $intervalo      = DateInterval::createFromDateString($seconds . ' seconds');
        
        $reservas       = Reserva::getReservasPorPeluqueroYDia($this->id, $fecha);

        //return $reservas;
        $date_inicio    = $this->getTimeUpdate($fecha, $date_inicio, $date_fin, $intervalo);

        if(count($reservas) == 0){
            return ["Intervalo" => $this->empresa->duracion_servicio,
                    "Blocks"    => $this->getFullBlocks($date_inicio,
                                                        $date_fin,
                                                        $intervalo)];
        }else{
            return ["Intervalo" => $this->empresa->duracion_servicio,
                    "Blocks"    => $this->getBlocks($date_inicio,
                                                    $date_fin,
                                                    $intervalo,
                                                    $reservas)];
        }
    }

    private function getTimeUpdate($fecha, $Inicio, $Fin, $Intervalo){
        $ahora = date_create();
        $tmpInicio = $Inicio;

        if(date_format($ahora,"Y-m-d") == $fecha){
            if($ahora <= $Inicio){
                return $Inicio;
            }else{
                while(($tmpInicio < $Fin) && ($tmpInicio < $ahora)){
                    date_add($tmpInicio, $Intervalo);
                }
                return $tmpInicio;
            }
        }else{
            return $Inicio;
        }
    }

    private function getFullBlocks($Inicio, $Fin, $Intervalo){
        $lista          = array();

        while($Inicio < $Fin){
            array_push($lista, date_format($Inicio,"H:i"));
            date_add($Inicio, $Intervalo);
        }

        return $lista;
    }

    private function getBlocks($Inicio, $Fin, $Intervalo, $Reservas){
        $lista          = array();
        $ini_tmp        = date_create(date_format($Inicio,"H:i"));
        $fin_tmp        = date_create(date_format($Inicio,"H:i"));
        date_add($fin_tmp, $Intervalo);

        foreach ($Reservas as $reserva) {
            $break = true;
            while ($break) {
                if($reserva->inicio >= date_format($fin_tmp,"H:i")){
                    array_push($lista, date_format($ini_tmp,"H:i"));
                    date_add($ini_tmp, $Intervalo);
                    date_add($fin_tmp, $Intervalo);
                }else{
                    $ini_tmp    = date_create($reserva->fin);
                    $fin_tmp    = date_create(date_format($ini_tmp,"H:i"));
                    date_add($fin_tmp, $Intervalo);
                    $break      = false;
                }
            }

        }

        while($ini_tmp < $Fin){
            array_push($lista, date_format($ini_tmp,"H:i"));
            date_add($ini_tmp, $Intervalo);
        }

        return $lista;
    }
}
