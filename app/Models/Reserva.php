<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Peluquero;
use App\Models\Cliente;

use DateInterval;

class Reserva extends Model
{
    use HasFactory;
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'peluquero_id',
        'cliente_id',
        'fecha',
        'inicio',
        'fin'
    ];

    public function user_cliente(){
        return $this->belongsTo(User::class,'cliente_id');
    }

    public function user_peluquero(){
        return $this->belongsTo(User::class,'peluquero_id');
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class)->with('user');
    }

    public function peluquero(){
        return $this->belongsTo(Peluquero::class)->with('user','empresa');
    }

    public static function getReservasMayoresADia($fecha){
        return Reserva::whereDate('fecha','>=',$fecha)
                        ->orderBy('inicio', 'asc')
                        ->get();
    }

    public static function getLicenciasMayoresADia($peluquero_id, $fecha){
        return Reserva::whereDate('fecha','>=',$fecha)
                        ->where('peluquero_id', $peluquero_id)
                        ->where('cliente_id',1)
                        ->orderBy('fecha', 'asc')
                        ->orderBy('inicio', 'asc')
                        ->get();
    }

    public static function getLicenciasDelDia($peluquero_id, $fecha){
        return Reserva::whereDate('fecha','=',$fecha)
                        ->where('peluquero_id', $peluquero_id)
                        ->where('cliente_id',1)
                        ->orderBy('fecha', 'asc')
                        ->orderBy('inicio', 'asc')
                        ->get();
    }

    public static function getReservasPeluqueroYDia($peluquero_id, $dia){
        return Reserva::where('peluquero_id', $peluquero_id)
                        ->where('cliente_id', "!=", 1)
                        ->whereDate('fecha','=',$dia)
                        ->with('cliente')
                        ->orderBy('inicio', 'asc')
                        ->get();
    }

    public static function getReservasPorPeluqueroYDia($peluquero_id, $dia){
        return Reserva::where('peluquero_id', $peluquero_id)
                        ->whereDate('fecha','=',$dia)
                        ->orderBy('inicio', 'asc')
                        ->get();
    }

    public static function createLicencia(Request $request){
        try {
            $peluquero      = Peluquero::where('id','=', $request->peluquero_id)
                                        ->with('empresa')
                                        ->first();

            $Fecha          = date_create($request->fecha);

            if(Reserva::esLaborable($Fecha, $peluquero->dias_nolaborables)){
                Reserva::create([
                    'peluquero_id'  => $request->peluquero_id,
                    'cliente_id'    => 1,
                    'fecha'         => date_format($Fecha,"Y-m-d"),
                    'inicio'        => $request->inicio,
                    'fin'           => $request->fin
                ]);
            }

            return ["Message" => "OK"];
        }
        catch (QueryException $qe) {
            //return ["Message" => $qe];
            return ["Message" => "ERROR", "Errores" => $qe];
        }
    }

    public static function createReserva(Request $request){
        try {
            $peluquero      = Peluquero::where('id','=', $request->peluquero_id)
                                        ->with('empresa')
                                        ->first();
            
            $seconds        = strval(60 * $peluquero->empresa->duracion_servicio);
            $intervalo      = DateInterval::createFromDateString($seconds . ' seconds');

            $fin_tmp        = date_create($request->inicio);
            date_add($fin_tmp, $intervalo);
            
            Reserva::create([
                'peluquero_id'  => $request->peluquero_id,
                'cliente_id'    => $request->cliente_id,
                'fecha'         => $request->fecha,
                'inicio'        => $request->inicio,
                'fin'           => date_format($fin_tmp,"H:i")
            ]);

            return ["Message" => "OK"];
        }
        catch (QueryException $qe) {
            //return ["Message" => $qe];
            return ["Message" => "ERROR", "Errores" => $qe];
        }
    }

    public static function createLicenciaRango(Request $request){
        try {
            $peluquero      = Peluquero::where('id','=', $request->peluquero_id)
                                        ->with('empresa')
                                        ->first();

            $Inicio_date    = date_create($request->fechaInicio);
            $Fin_date       = date_create($request->fechaFin);
            
            while($Inicio_date <= $Fin_date){
                if(Reserva::esLaborable($Inicio_date, $peluquero->dias_nolaborables)){
                    Reserva::create([
                        'peluquero_id'  => $request->peluquero_id,
                        'cliente_id'    => 1,
                        'fecha'         => date_format($Inicio_date,"Y-m-d"),
                        'inicio'        => "00:00",
                        'fin'           => "23:59"
                    ]);
                }
                $Inicio_date->modify('+1 day');
            }

            return ["Message" => "OK"];
        }
        catch (QueryException $qe) {
            //return ["Message" => $qe];
            return ["Message" => "ERROR", "Errores" => $qe];
        }
    }
    private static function esLaborable($fecha, $listaNoLaborable){
        $result = true;
        foreach(explode(",",$listaNoLaborable) as $diaNoLaborable){
            $numDia = date('w', strtotime(date_format($fecha, "Y-m-d")));
            if($diaNoLaborable == $numDia) {
                $result = false;
            }
        }
        return $result;
    }
}
