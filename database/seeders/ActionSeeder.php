<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Action;
use App\Models\Rol;

class ActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $adm = Rol::where('id',1)->get()->first();
        $pel = Rol::where('id',2)->get()->first();
        $cli = Rol::where('id',3)->get()->first();

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Home",
            'descripcion'   => "Pantalla de inicio."
        ]);
        $act = Action::where('nombre','Home')->get();
        $adm->actions()->attach($act);
        $pel->actions()->attach($act);
        $cli->actions()->attach($act);

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Reserva",
            'descripcion'   => "Reserva simple de sesiones."
        ]);
        $act = Action::where('nombre','Reserva')->get();
        $cli->actions()->attach($act);

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Reserva_Especial",
            'descripcion'   => "Reserva de sesiones a Clientes."
        ]);
        $act = Action::where('nombre','Reserva_Especial')->get();
        $adm->actions()->attach($act);
        $pel->actions()->attach($act);

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Mis_Reservas",
            'descripcion'   => "Lista de Reservas del usuario."
        ]);
        $act = Action::where('nombre','Mis_Reservas')->get();
        $cli->actions()->attach($act);

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Mis_Horarios",
            'descripcion'   => "Calendario con la actividad del Peluquero."
        ]);
        $act = Action::where('nombre','Mis_Horarios')->get();
        $pel->actions()->attach($act);

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Estadisticas",
            'descripcion'   => "Estadisticas de Reservas."
        ]);
        $act = Action::where('nombre','Estadisticas')->get();
        $adm->actions()->attach($act);

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Configuraciones",
            'descripcion'   => "Panel de configuraciones del sistema."
        ]);
        $act = Action::where('nombre','Configuraciones')->get();
        $adm->actions()->attach($act);

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Cambio_Password",
            'descripcion'   => "Panel de cambio de contraseña a los usuarios."
        ]);
        $act = Action::where('nombre','Cambio_Password')->get();
        $adm->actions()->attach($act);

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Actualizar_Password",
            'descripcion'   => "Panel de actualizacion de contraseña."
        ]);
        $act = Action::where('nombre','Actualizar_Password')->get();
        $adm->actions()->attach($act);
        $pel->actions()->attach($act);
        $cli->actions()->attach($act);

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Alta_Administradores",
            'descripcion'   => "Dar de alta Administradores."
        ]);
        $act = Action::where('nombre','Alta_Administradores')->get();
        $adm->actions()->attach($act);

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Alta_Peluqueros",
            'descripcion'   => "Dar de alta Peluqueros."
        ]);
        $act = Action::where('nombre','Alta_Peluqueros')->get();
        $adm->actions()->attach($act);

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Eliminar_Peluqueros",
            'descripcion'   => "Eliminar Peluqueros."
        ]);
        $act = Action::where('nombre','Eliminar_Peluqueros')->get();
        $adm->actions()->attach($act);

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Alta_Clientes",
            'descripcion'   => "Dar de alta Clientes."
        ]);
        $act = Action::where('nombre','Alta_Clientes')->get();
        $adm->actions()->attach($act);
        $pel->actions()->attach($act);

        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Eliminar_Clientes",
            'descripcion'   => "Eliminar Clientes."
        ]);
        $act = Action::where('nombre','Eliminar_Clientes')->get();
        $adm->actions()->attach($act);
        $pel->actions()->attach($act);

        //------------------------------------------------------------
        //------------------------------------------------------------
        Action::create([
            'nombre'        => "Licencias",
            'descripcion'   => "Gestion de Licencia de Peluqueros."
        ]);
        $act = Action::where('nombre','Licencias')->get();
        $adm->actions()->attach($act);
        $pel->actions()->attach($act);

        //------------------------------------------------------------
    }
}
