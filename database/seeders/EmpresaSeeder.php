<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Empresa;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empresa::create([
            'nombre'            => "Cortes K",
            'direccion'         => "Rafael Pastoriza 1350, 11300 Montevideo, Departamento de Montevideo",
            'ubicacion'         => "-34.90444231568152 -56.15178348977024",
            'mensaje'           => "Mensaje de bienvenida!",
            'duracion_servicio' => 20
        ]);
    }
}
