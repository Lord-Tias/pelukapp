<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Rol;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ROL::create([
            'nombre'        => "Administrador",
            'descripcion'   => "Usuario con todos los privilegios."
        ]);

        ROL::create([
            'nombre'        => "Peluquero",
            'descripcion'   => "Usuario con algunos privilegios limitados."
        ]);

        ROL::create([
            'nombre'        => "Cliente",
            'descripcion'   => "Usuario con el privilegio de hacer reservas."
        ]);
    }
}
