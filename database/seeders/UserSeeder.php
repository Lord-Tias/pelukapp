<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Peluquero;
use App\Models\Cliente;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userC = User::create([
            'nombre'        => "Licencia",
            'apellido'      => "",
            'email'         => "lic@lic.com",
            'password'      => Hash::make("qwer"),
            'rol_id'        => 3 /* Cliente */
        ]);
        Cliente::create([
            'id'            => $userC->id,
            'telefono'      => "092312370",
            'nacimiento'    => "1988-03-27"
        ]);
        User::create([
            'email'         => "admin@admin.com",
            'password'      => Hash::make("qwer"),
            'nombre'        => "Administrador",
            'apellido'      => "Sistema",
            'rol_id'        => "1"
        ]);
        $userPE = User::create([
            'nombre'        => "Eduardo",
            'apellido'      => "Kerves",
            'email'         => "eduardo@peluquero.com",
            'password'      => Hash::make("qwer"),
            'rol_id'        => 2 /* Peluquero */
        ]);

        Peluquero::create([
            'id'                => $userPE->id,
            'dias_nolaborables' => "0",
            'inicio_actividad'  => "09:00",
            'fin_actividad'     => "18:00",
            'empresa_id'        => 1
        ]);

        $userPS = User::create([
            'nombre'        => "Santiago",
            'apellido'      => "Kerves",
            'email'         => "santiago@peluquero.com",
            'password'      => Hash::make("qwer"),
            'rol_id'        => 2 /* Peluquero */
        ]);
        Peluquero::create([
            'id'                => $userPS->id,
            'dias_nolaborables' => "0,6",
            'inicio_actividad'  => "09:00",
            'fin_actividad'     => "18:00",
            'empresa_id'        => 1
        ]);

        $userC = User::create([
            'nombre'        => "Cliente 1",
            'apellido'      => "Test",
            'email'         => "clie@clie.com",
            'password'      => Hash::make("qwer"),
            'rol_id'        => 3 /* Cliente */
        ]);
        Cliente::create([
            'id'            => $userC->id,
            'telefono'      => "092312370",
            'nacimiento'    => "1988-03-27"
        ]);
        $userC = User::create([
            'nombre'        => "Cliente 2",
            'apellido'      => "Test",
            'email'         => "clie2@clie.com",
            'password'      => Hash::make("qwer"),
            'rol_id'        => 3 /* Cliente */
        ]);
        Cliente::create([
            'id'            => $userC->id,
            'telefono'      => "092312370",
            'nacimiento'    => "1988-03-27"
        ]);
    }
}
