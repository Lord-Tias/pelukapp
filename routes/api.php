<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Login_Controller;
use App\Http\Controllers\Peluquero_Controller;
use App\Http\Controllers\Cliente_Controller;
use App\Http\Controllers\Reserva_Controller;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//MODELO PELUQUERO
Route::middleware('auth')->post('/registroPeluquero',
                                [Peluquero_Controller::class,   'Registrar']);
Route::middleware('auth')->get('/getPeluqueros',
                                [Peluquero_Controller::class,   'getPeluqueros']);
Route::middleware('auth')->post('/getHorasLibres',
                                [Peluquero_Controller::class,   'getHorasLibres']);
Route::middleware('auth')->get('/getPeluqueroReservas',
                                [Peluquero_Controller::class,   'getPeluqueroReservas']);
Route::middleware('auth')->post('/getPeluqueroReservasPorDia',
                                [Peluquero_Controller::class,   'getPeluqueroReservasPorDia']);
Route::middleware('auth')->post('/deletePeluquero',
                                [Peluquero_Controller::class,     'deletePeluquero']);

//MODELO CLIENTE
Route::post('/registroCliente',
                                [Cliente_Controller::class,     'Registrar']);
Route::middleware('auth')->get('/getClienteReservas',
                                [Cliente_Controller::class,     'getClienteReservas']);
Route::middleware('auth')->get('/getClientes',
                                [Cliente_Controller::class,     'getClientes']);
Route::middleware('auth')->post('/deleteCliente',
                                [Cliente_Controller::class,     'deleteCliente']);

//MODELO RESERVA
Route::middleware('auth')->post('/registroReserva',
                                [Reserva_Controller::class,     'Registrar']);
Route::middleware('auth')->post('/deleteReserva',
                                [Reserva_Controller::class,     'deleteReserva']);
Route::middleware('auth')->get('/getReservas',
                                [Reserva_Controller::class,     'getReservas']);
Route::middleware('auth')->post('/RegistrarLicenciaRango',
                                [Reserva_Controller::class,     'RegistrarLicenciaRango']);
Route::middleware('auth')->post('/RegistrarLicencia',
                                [Reserva_Controller::class,     'RegistrarLicencia']);
Route::middleware('auth')->post('/getLicencias',
                                [Reserva_Controller::class,     'getLicencias']);
Route::middleware('auth')->post('/getLicenciasPorFecha',
                                [Reserva_Controller::class,     'getLicenciasPorFecha']);
Route::middleware('auth')->post('/deleteMultiple',
                                [Reserva_Controller::class,     'deleteMultiple']);

//LOGIN
Route::middleware('auth')->post('/registro',
                                [Login_Controller::class,       'Registrar']);
Route::middleware('auth')->post('/CambioContraseña',
                                [Login_Controller::class,       'CambioContraseña']);
Route::middleware('auth')->post('/CambioContraseñaAdmin',
                                [Login_Controller::class,       'CambioContraseñaAdmin']);
Route::post('/login',
                                [Login_Controller::class,       'Login']);
Route::middleware('auth')->post('/logout',
                                [Login_Controller::class,       'Logout']);
Route::middleware('auth')->get('/user',
                                function (Request $request) {
    return $request->user();
});